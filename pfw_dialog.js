/**
 * @file
 * Provides Jquery UI dialog functionality to PFW elements.
 */

(function ($) {
  Drupal.behaviors.pfwDialog = {
    attach: function (context, settings) {

      // jQuery UI escapes HTML in dialog titles - we want markup in there.
      $.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
        _title: function (title) {
          if (!this.options.title) {
            // If there's no title, add a non-breaking space.
            title.html("&#160;");
          }
          else {
            title.html(this.options.title);
          }
        }
      }));

      $('a.dialog, li.dialog a', context).click(function(event) {
        event.preventDefault();

        var $link = $(this);
        var $dialog = $('<div id="dialog-container"></div>');
        var href = $link.attr('href');

        // Stripping out protocol, domain etc for absolute URLs
        var relativeSrc = href.replace(/https?:\/\/[^\/]+/i, "");
        var src = '/pfw_dialog' + relativeSrc;

        var dialogWidth = 900;
        var dialogTitle = '';

        if ($link.hasClass('dialog-narrow')) {
          dialogWidth = 700;
        }

        $.getJSON(src, function (data) {
          $dialog.html(data.body);

          // If the dialog window relates to track and trace help include a H2 element to improve markup.
          if ($link.hasClass('track-trace-help')) {
            dialogTitle = '<h1>' + data.title + '</h1><h2 class="element-invisible">Track and trace help</h2>';
          }
          else {
            dialogTitle = '<h1>' + data.title + '</h1>';
          }

          $dialog.dialog({
            modal: true,
            autoOpen: false,
            title: dialogTitle,
            draggable: 'false',
            resizable: 'disable',
            width: dialogWidth,
            dialogClass: 'color-white',

            // Return focus to the previously clicked dialog link.
            close: function () {
              $('.focussed').focus().removeClass('focussed');
              $('.ui-dialog').remove();
            }
          }).parent().draggable({disabled:true});
          $dialog.dialog('open');

          var $uiDialog = $('.ui-dialog');
          var dialogTop = parseInt(($uiDialog).css('top'), 10);

          // If the dialog top position is a negative value reposition the overlay to avoid the close button being cropped.
          if (dialogTop < 0) {
            $uiDialog.css('top', '0px');
          }

          $link.addClass('focussed');
          $uiDialog.removeAttr('role').find('button').blur();
          $('.ui-widget-overlay').css({'background':'#333', 'opacity':'0.8'});
          $('.ui-dialog a').each(function() {
            if ($(this).attr('target') == '_blank') {
              $(this).addClass('extlink');
            }
          });
          return false;
        });
      });
    }
  };

/**
 * Behaviour pfwDialogDOMSrc
 *
 * Adds functionality to allow defining dialogs that use a part of the DOM as their
 * content source rather than a URL. pfwDialog could be modified to add this
 * functionality but for now a separate behaviour is used to prevent side effects.
 *
 * Usage:
 * Give the dialog source element the class ".dialog-dom"
 * Give the same element a unique "id"
 * Give the "a" element (or the li that contains the "a" element) that is to be the link
 * a rel attribute with value of the unique "id" of the source element
 */
Drupal.behaviors.pfwDialogDOMSrc = {
  attach: function (context, settings) {

    var overlays = [];

    $('.dialog-dom', context).once('pfwDialogDOMSrc', function () {
      // Create overlays.
      $('.dialog-dom', context).each(function(index) {
        var id = $(this).attr('id');
        overlays[id] = $(this);
        overlays[id].dialog({
          modal: true,
          autoOpen: false,
          title: '<h1>' + $(this).attr('title') + '</h1>',
          draggable: 'false',
          resizable: 'disable',
          width: 600,
          dialogClass: 'color-white',

          // Return focus to the previously clicked dialog link.
          close: function () {
            $('.focussed').focus().removeClass('focussed');
            $(".ui-dialog").attr('aria-disabled', true);
          }
        }).parent().draggable({disabled:true});
        // this may not be correct here.
      });

      // Activate overlay links
      $('a.dialog-dom-link, li.dialog-dom-link a', context).each(function(index) {
        var $link = $(this);
        $link.click(function (event) {
          $link.addClass('focussed');
          // rel attribute provides the overlays array key (which is also the element #) that references the dialog source element
          var id = $(this).attr('rel');
          overlays[id].data('triggerElement', this);
          overlays[id].dialog('open');
          $('.ui-dialog').removeAttr('role').focus().attr('aria-disabled', false);
          $('.ui-widget-overlay').css({'background':'#333', 'opacity':'0.8'});
          return false;
        });
      });
    });
  }
}

}(jQuery));
