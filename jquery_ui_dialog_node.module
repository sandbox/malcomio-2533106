<?php
/**
 * @file
 * Display the contents of a node in a jQuery UI dialog box.
 *
 * Takes some ideas from http://drupal.org/project/colorbox_node
 */

/**
 * Implements hook_menu().
 */
function jquery_ui_dialog_node_menu() {
  $items['jquery_ui_dialog_node/%jquery_ui_dialog_node_node_url'] = array(
    'page callback' => 'jquery_ui_dialog_node_output',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
    'load arguments' => array('%map'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Wildcard loader to get the path to use in the dialog.
 *
 * Takes a path as an array of all our arguments from the
 * url structure.  We then put that structure back together,
 * find our system path and then return it.
 */
function jquery_ui_dialog_node_node_url_load($arg, $path = array()) {
  // Remove jquery_ui_dialog_node from the path.
  array_shift($path);

  // Lets determine if we have a prefix from our languages.
  if (module_exists('locale') && function_exists('language_url_split_prefix')) {
    // Get our language list to pass into our url splitter.
    $languages = language_list();
    // Turn the path into a string so we can then remove our prefix.
    $path_string = implode('/', $path);
    $language_path = language_url_split_prefix($path_string, $languages);
    // Separate out the returned path and language.  We should always
    // have two indexes, the first is the language, second is the path.
    $language = $language_path[0] ? $language_path[0]->language : '';
    $final_path = $language_path[1];
    // Lets find our path based on the current translation.
    return drupal_get_normal_path($final_path, $language);
  }
  // Now lets buld our path to return our system path,
  return drupal_get_normal_path(implode('/', $path));
}

/**
 * Page callback to display the additional cookies information.
 */
function jquery_ui_dialog_node_output($path) {

  // Fetch our callback based on our path.
  $page_callback_result = menu_execute_active_handler($path, FALSE);

  if (isset($page_callback_result['nodes'])) {
    $node_array = reset($page_callback_result['nodes']);

    $node = $node_array['#node'];

    $body = field_get_items('node', $node, 'body');
    $display_title = field_get_items('node', $node, 'field_display_title');

    $output = array(
      'title' => filter_xss($display_title[0]['value']),
      'body' => check_markup($body[0]['value'], $body[0]['format']),
    );
  }

  return drupal_json_output($output);
}

/**
 * Implements template_preprocess_block().
 */
function jquery_ui_dialog_node_preprocess_page(&$vars, $hook) {
  // Loading needed libraries.
  drupal_add_library('system', 'ui.dialog');
  drupal_add_js(drupal_get_path('module', 'jquery_ui_dialog_node') . '/jquery_ui_dialog_node.js');
}
