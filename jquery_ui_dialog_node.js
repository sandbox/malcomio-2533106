/**
 * @file
 * Provides Jquery UI dialog functionality.
 */

(function ($) {
  Drupal.behaviors.jqueryUiDialogNode = {
    attach: function (context, settings) {

      // jQuery UI escapes HTML in dialog titles - we want markup in there.
      $.widget("ui.dialog", $.extend({}, $.ui.dialog.prototype, {
        _title: function (title) {
          if (!this.options.title) {
            // If there's no title, add a non-breaking space.
            title.html("&#160;");
          }
          else {
            title.html(this.options.title);
          }
        }
      }));

      $('a.dialog, li.dialog a', context).click(function(event) {
        event.preventDefault();

        var $link = $(this);
        var $dialog = $('<div id="dialog-container"></div>');
        var href = $link.attr('href');

        // Stripping out protocol, domain etc for absolute URLs
        var relativeSrc = href.replace(/https?:\/\/[^\/]+/i, "");
        relativeSrc = relativeSrc.replace(Drupal.settings.basePath, '');
        var src = Drupal.settings.basePath + 'pfw_dialog/' + relativeSrc;

        var dialogWidth = 900;
        var dialogTitle = '';

        if ($link.hasClass('dialog-narrow')) {
          dialogWidth = 700;
        }

        $.getJSON(src, function (data) {
          $dialog.html(data.body);

          dialogTitle = '<h1>' + data.title + '</h1>';

          $dialog.dialog({
            modal: true,
            autoOpen: false,
            title: dialogTitle,
            draggable: 'false',
            resizable: 'disable',
            width: dialogWidth,
            dialogClass: 'color-white',

            // Return focus to the previously clicked dialog link.
            close: function () {
              $('.focussed').focus().removeClass('focussed');
              $('.ui-dialog').remove();
            }
          }).parent().draggable({disabled:true});
          $dialog.dialog('open');

          // Attach js to dynamically created elements.
          Drupal.attachBehaviors(null, Drupal.settings);

          var $uiDialog = $('.ui-dialog');
          var dialogTop = parseInt(($uiDialog).css('top'), 10);

          // If the dialog top position is a negative value reposition the overlay to avoid the close button being cropped.
          if (dialogTop < 0) {
            $uiDialog.css('top', '0px');
          }

          $link.addClass('focussed');
          $uiDialog.removeAttr('role').find('button').blur();
          $('.ui-widget-overlay').css({'background':'#333', 'opacity':'0.8'});
          $('.ui-dialog a').each(function() {
            if ($(this).attr('target') == '_blank') {
              $(this).addClass('extlink');
            }
          });
          return false;
        });
      });
    }
  };

}(jQuery));
